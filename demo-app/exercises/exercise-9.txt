Exercise 9

GitLab: https://gitlab.com/t4d-classes/react-redux_06102019

1. Move Car Form to its own component. Utilize Car Form in Car Tool.

2. Make the button text customizable for the Car Form. Set the button text to 'Add Car'.

3. Replace the color input with a select control. List the following options: red, blue, black, white, & silver. The options should be passed in as props to the car form component.

5. Utilize the custom form hook to the manage form state.

6. Ensure everything works.