import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { calcToolReducer } from './CalcTool/reducers/calcToolReducer';
import { carToolReducer } from './CarTool/reducers/carToolReducer';

export const toolStore = createStore(
  combineReducers({
    calcTool: calcToolReducer,
    carTool: carToolReducer,
  }),
  composeWithDevTools(applyMiddleware(thunk)),
);