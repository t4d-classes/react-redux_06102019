import { OP_ERROR } from '../actions/errorActions';

export const opErrorReducer = (state = null, action) => {

  if (action.type === OP_ERROR) {
    return action.payload;
  }

};