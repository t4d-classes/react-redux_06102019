export const OP_ERROR = 'OP_ERROR';

export const createOpErrorAction = (value) => ({ type: OP_ERROR, payload: value });