import React from 'react';
import { Link } from 'react-router-dom';

import './ToolMenu.css';

export const ToolMenu = () => {
  return <nav>
    <ul>
      <li><Link to="/">Home</Link></li>
      <li><Link to="/calc-tool">Calc Tool</Link></li>
      <li><Link to="/color-tool">Color Tool</Link></li>
      <li><Link to="/car-tool">Car Tool</Link></li>
    </ul>
  </nav>;
};