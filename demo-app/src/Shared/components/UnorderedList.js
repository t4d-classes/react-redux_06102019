import React from 'react';
import PropTypes from 'prop-types';


export const UnorderedList = ({ items = [] }) => {
  return <ul>
    {items.map((item, index) => <li key={index}>{item}</li>)}
  </ul>
};

UnorderedList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.string),
};

