import React from 'react';


export const ToolFooter = ({ companyName }) => {

  return <footer style={{ textAlign: 'center', fontSize: '12px' }}>
    <small>Copyright {new Date().getFullYear()}, {companyName}</small>
  </footer>;


};