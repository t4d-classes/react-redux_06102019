import React from 'react';
import PropTypes from 'prop-types';
import { isNil, isFunction } from 'lodash/fp';

export const ToolHeader = ({ component: Component, render, children, ...props }) => {

  const otherProps = { data: 'some data', ...props };

  if (isFunction(children)) {
    return <header>{children(otherProps)}</header>;
  }

  if (isFunction(render)) {
    return <header>{render(otherProps)}</header>;
  }

  if (isFunction(Component)) {
    return <header>
      <Component {...otherProps} />
    </header>;
  }

  if (!isNil(children)) {
    return <header>
      {children}
    </header>;
  }

};

ToolHeader.defaultProps = {
  children: <h1>The Tool</h1>,
};

ToolHeader.propTypes = {
  render: PropTypes.func,
  component: PropTypes.element,
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.element]),
};