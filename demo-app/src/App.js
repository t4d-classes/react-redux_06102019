import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { ToolHeader, ToolMenu, ToolFooter } from './Shared/components';

import { CalcToolContainer } from './CalcTool/containers/CalcToolContainer';
import { ColorTool } from './ColorTool/components/ColorTool';
import { CarToolContainer } from './CarTool/containers/CarToolContainer';

const colorList = [
  { id: 1, name: 'red', hexcode: '#FF0000' },
  { id: 2, name: 'green', hexcode: '#00FF00' },
  { id: 3, name: 'blue', hexcode: '#0000FF' },
];


export const App = () => {

  return <>
    <ToolHeader>
      <h1>The Tool</h1>
    </ToolHeader>
    <ToolMenu />
    <Switch>
      <Route path="/" exact render={() => <h2>Welcome!</h2>} />
      <Route path="/calc-tool" component={CalcToolContainer} />
      <Route path="/color-tool" exact render={() => <ColorTool colors={colorList} />} />
      <Route path="/car-tool" exact component={CarToolContainer} />
    </Switch>
    <ToolFooter companyName="A Cool Company, Inc." />
  </>;
}