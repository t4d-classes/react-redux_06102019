import { createOpErrorAction } from '../../Shared/actions/errorActions';

export const ADD_ACTION = '[CalcTool] ADD';
export const SUBTRACT_ACTION = '[CalcTool] SUBTRACT';
export const OP_ERROR = '[CalcTool] OP_ERROR';
export const REFRESH_HISTORY_REQUEST = '[CalcTool] Refresh History Request';
export const REFRESH_HISTORY_DONE = '[CalcTool] Refresh History Done';
export const APPEND_HISTORY_REQUEST = '[CalcTool] Append History Request';
export const APPEND_HISTORY_DONE = '[CalcTool] Append History Done';

export const createAddAction = value => {

  if (isNaN(value)) {
    return createOpErrorAction('Not a number');
  }

  return ({ type: ADD_ACTION, payload: value });
};

export const createSubtractAction = value => ({ type: SUBTRACT_ACTION, payload: value });

export const createRefreshHistoryRequestAction = () => ({ type: REFRESH_HISTORY_REQUEST });
export const createRefreshHistoryDoneAction = (history) => ({ type: REFRESH_HISTORY_DONE, payload: { history }});

export const createAppendHistoryRequestAction = () => ({ type: APPEND_HISTORY_REQUEST });
export const createAppendHistoryDoneAction = (historyEntry) => ({ type: APPEND_HISTORY_DONE, payload: { historyEntry }});

export const refreshHistory = () => {

  return (dispatch) => {
    dispatch(createRefreshHistoryRequestAction());
    return fetch('http://localhost:3050/history')
      .then(res => res.json())
      .then(history => dispatch(createRefreshHistoryDoneAction(history)));
  };
};

export const add = (value) => {
  return dispatch => {
    dispatch(createAddAction(value));
    dispatch(createAppendHistoryRequestAction());
    return fetch('http://localhost:3050/history', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ name: 'ADD', value }),
    })
      .then(dispatch(refreshHistory()));

  };
}

