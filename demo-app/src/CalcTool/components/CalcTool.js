import React, { useState, useEffect } from 'react';

import { UnorderedList } from '../../Shared/components/UnorderedList';

export const CalcTool = ({
  result, history, historyLen, errorMessage, actionCount,
  onAdd: add, onSubtract: subtract, onRefreshHistory: refreshHistory,
  onNoOp: noop,
}) => {

  const [ numInput, setNumInput ] = useState(0);

  useEffect(() => {
    refreshHistory();
  }, []);

  return <>
    {errorMessage && <div>Error Message: {errorMessage}</div>}
    <div className="some-class" style={{color: 'red'}}>Action Count: {actionCount}</div>
    <form>
      <div>
        Result: {result}
      </div>
      <div>
        Input: <input type="text" value={numInput}
          onChange={e => setNumInput(Number(e.target.value))} />
      </div>
      <div>
        <button type="button" onClick={() => add(numInput)}>+</button>
        <button type="button" onClick={() => subtract(numInput)}>-</button>
        <button type="button" onClick={noop}>NoOp</button>
      </div>
    </form>
    <button type="button" onClick={refreshHistory}>Refresh History</button>
    <UnorderedList items={history.map(h => h.name + ' ' + h.value)} />
    <div>History Items: {historyLen}</div>
  </>;

}
