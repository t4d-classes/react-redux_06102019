import { REFRESH_HISTORY_DONE } from '../actions/calcActions';

export const historyReducer = (state = [], action) => {
  switch(action.type) {
    case REFRESH_HISTORY_DONE:
      return action.payload.history;
    default:
      return state;
  }
}
