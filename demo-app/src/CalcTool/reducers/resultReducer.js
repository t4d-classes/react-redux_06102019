
import { ADD_ACTION, SUBTRACT_ACTION } from '../actions/calcActions';

export const resultReducer = (state = 0, action) => {

  switch(action.type) {
    case ADD_ACTION:
      return state + action.payload;
    case SUBTRACT_ACTION:
      return state - action.payload;
    default:
      return state;
  }

};