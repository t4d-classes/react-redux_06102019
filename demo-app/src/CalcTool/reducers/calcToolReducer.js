import { resultReducer } from './resultReducer';
import { historyReducer } from './historyReducer';
import { opErrorReducer } from '../../Shared/reducers/opErrorReducer';

export const calcToolReducer = (state = { actionCount: 0 }, action) => {

  return {
    ...state,
    // features,
    actionCount: state.actionCount + 1,
    result: resultReducer(state.result, action),
    history: historyReducer(state.history, action),
    opError: opErrorReducer(state.opError, action),
  };

};