import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { CalcTool } from '../components/CalcTool';

import { add, createSubtractAction, refreshHistory } from '../actions/calcActions';

const getLength = list => list.length;

const createCalcToolContainer = connect(
  ({ calcTool: { result, history, actionCount, opError }}) => ({
    result: result,
    history: history,
    actionCount: actionCount,
    historyLen: getLength(history),
    errorMessage: opError,
  }),
  (dispatch) => bindActionCreators({
    onRefreshHistory: refreshHistory,
    onAdd: add,
    onSubtract: createSubtractAction,
    onNoOp: () => ({ type: 'NOOP' }),
  }, dispatch),
);

export const CalcToolContainer = createCalcToolContainer(CalcTool);