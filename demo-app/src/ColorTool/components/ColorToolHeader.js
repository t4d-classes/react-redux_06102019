import React from 'react';

import { ToolHeader } from '../../Shared/components/ToolHeader';

export const ColorToolHeader = () => {

  return <ToolHeader render={() => <h1>Color Tool</h1>} />;

};