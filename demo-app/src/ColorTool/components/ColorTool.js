import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { map } from 'lodash/fp';

import { ColorToolHeader } from './ColorToolHeader';
import { UnorderedList } from '../../Shared/components/UnorderedList';
import { ColorForm } from './ColorForm';

import { nextId, copy } from '../../Shared/utils';

const mapColorNames = map(color => color.name);

export const ColorTool = ({ colors: initialColors }) => {

  const [ colors, setColors ] = useState(copy(initialColors));

  const addColor = (color) => {
    setColors(colors.concat({
      ...color,
      id: nextId(colors),
    }));
  };

  // const headerText = 'In Color Tool';

  return <>
    <ColorToolHeader />
    <UnorderedList items={mapColorNames(colors)} />
    <ColorForm buttonText="Add Color" onSubmitColor={addColor} />
  </>;
}

ColorTool.propTypes = {
  colors: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    hexcode: PropTypes.string,
  })),
};
