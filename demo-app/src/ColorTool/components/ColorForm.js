import React from 'react';
import PropTypes from 'prop-types';

import { useForm } from '../../Shared/hooks/useForm';

export const ColorForm = ({
  buttonText = 'Submit Color',
  onSubmitColor,
}) => {

  const [ colorForm, change, resetColorForm ] = useForm({
    name: '',
    hexcode: '',
  });

  const submitColor = () => {

    onSubmitColor({
      ...colorForm,
    });

    resetColorForm();
  };

  return <form>
    <div>
      <label htmlFor="name-input">Name:</label>
      <input type="text" id="name-input" name="name"
        value={colorForm.name} onChange={change} />
    </div>
    <div>
      <label htmlFor="hexcode-input">HexCode:</label>
      <input type="text" id="hexcode-input" name="hexcode"
        value={colorForm.hexcode} onChange={change} />
    </div>
    <button type="button" onClick={submitColor}>{buttonText}</button>
  </form>;

};

ColorForm.propTypes = {
  buttonText: PropTypes.string,
  onSubmitColor: PropTypes.func.isRequired,
};