import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import { toolStore } from './toolStore';
import { App } from './App';

ReactDOM.render(
  <Router>
    <Provider store={toolStore}>
      <App />
    </Provider>
  </Router>,
  document.querySelector('#root'),
);
