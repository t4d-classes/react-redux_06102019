import PropTypes from 'prop-types';

export const carTableConfigPropTypes = PropTypes.arrayOf(PropTypes.shape({
  field: PropTypes.string,
  caption: PropTypes.shape,
  type: PropTypes.string,
}));