import { refreshCars } from './refreshCars';

export const APPEND_CAR_REQUEST_ACTION = '[Car Tool] Append Car Request';
export const APPEND_CAR_DONE_ACTION = '[Car Tool] Append Car Done';

export const createAppendCarRequestAction = () => ({ type: APPEND_CAR_REQUEST_ACTION });
export const createAppendCarDoneAction = car => ({ type: APPEND_CAR_DONE_ACTION, payload: { car } });

export const appendCar = car => {
  return (dispatch) => {
    dispatch(createAppendCarRequestAction());
    return fetch('http://localhost:3050/cars', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(car),
    })
      .then(res => res.json())
      .then(car => {
        dispatch(createAppendCarDoneAction(car));
        dispatch(refreshCars());
      });
  };
};