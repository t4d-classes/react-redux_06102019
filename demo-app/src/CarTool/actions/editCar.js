export const EDIT_CAR_ACTION = '[Car Tool] Edit Car';
export const CANCEL_CAR_ACTION = '[Car Tool] Cancel Car';

export const createEditCarAction = carId => ({ type: EDIT_CAR_ACTION, payload: { carId }});
export const createCancelCarAction = () => ({ type: CANCEL_CAR_ACTION });