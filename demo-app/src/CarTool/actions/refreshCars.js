export const REFRESH_CARS_REQUEST_ACTION = '[Car Tool] Refresh Cars Request';
export const REFRESH_CARS_DONE_ACTION = '[Car Tool] Refresh Cars Done';

export const createRefreshCarsRequestAction = () => ({ type: REFRESH_CARS_REQUEST_ACTION });
export const createRefreshCarsDoneAction = cars => ({ type: REFRESH_CARS_DONE_ACTION, payload: { cars } });

export const refreshCars = () => {
  return (dispatch) => {
    dispatch(createRefreshCarsRequestAction());
    return fetch('http://localhost:3050/cars')
      .then(res => res.json())
      .then(cars => dispatch(createRefreshCarsDoneAction(cars)));
  };
};