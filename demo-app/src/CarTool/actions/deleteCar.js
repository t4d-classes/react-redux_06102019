import { refreshCars } from './refreshCars';

export const DELETE_CAR_REQUEST_ACTION = '[Car Tool] Delete Car Request';
export const DELETE_CAR_DONE_ACTION = '[Car Tool] Delete Car Done';

export const createDeleteCarRequestAction = (carId) => ({ type: DELETE_CAR_REQUEST_ACTION, payload: { carId } });
export const createDeleteCarDoneAction = () => ({ type: DELETE_CAR_DONE_ACTION });

export const deleteCar = carId => {
  return (dispatch) => {
    dispatch(createDeleteCarRequestAction(carId));
    return fetch(`http://localhost:3050/cars/${encodeURIComponent(carId)}`, {
      method: 'DELETE',
    })
      .then(() => {
        dispatch(createDeleteCarDoneAction());
        dispatch(refreshCars());
      });
  };
};