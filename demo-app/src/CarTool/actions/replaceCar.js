import { refreshCars } from './refreshCars';

export const REPLACE_CAR_REQUEST_ACTION = '[Car Tool] Replace Car Request';
export const REPLACE_CAR_DONE_ACTION = '[Car Tool] Replace Car Done';

export const createReplaceCarRequestAction = car => ({ type: REPLACE_CAR_REQUEST_ACTION, payload: { car } });
export const createReplaceCarDoneAction = () => ({ type: REPLACE_CAR_DONE_ACTION });

export const replaceCar = car => {
  return (dispatch) => {
    dispatch(createReplaceCarRequestAction(car));
    return fetch(`http://localhost:3050/cars/${encodeURIComponent(car.id)}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(car),
    })
      .then(() => {
        dispatch(createReplaceCarDoneAction());
        dispatch(refreshCars());
      });
  };
};