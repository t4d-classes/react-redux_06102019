import { EDIT_CAR_ACTION, CANCEL_CAR_ACTION } from '../actions/editCar';


export const editCarIdReducer = (state = -1, action) => {

  if (action.type === EDIT_CAR_ACTION) {
    return action.payload.carId;
  };

  if (action.type.endsWith(' Request') || action.type === CANCEL_CAR_ACTION) {
    return -1;
  }

  return state;
}