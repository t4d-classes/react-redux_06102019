import { REFRESH_CARS_DONE_ACTION } from '../actions/refreshCars';

export const carsReducer = (state = [], action) => {
  
  if (action.type === REFRESH_CARS_DONE_ACTION) {
    return action.payload.cars;
  }

  return state;
}