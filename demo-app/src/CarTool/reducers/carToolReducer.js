import { combineReducers } from 'redux';

import { carsReducer } from './carsReducer';
import { editCarIdReducer } from './editCarIdReducer';

export const carToolReducer = combineReducers({
  cars: carsReducer,
  editCarId: editCarIdReducer,
});