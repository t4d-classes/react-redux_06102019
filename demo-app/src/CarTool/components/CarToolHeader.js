import React from 'react';
import PropTypes from 'prop-types';

import { ToolHeader } from '../../Shared/components/ToolHeader';

export const CarToolHeader = ({ version }) => {

  return <ToolHeader>
    <h1>Car Tool <small>{version}</small></h1>
  </ToolHeader>

};

CarToolHeader.defaultProps = {
  version: '1.0',
};

CarToolHeader.propTypes = {
  version: PropTypes.string,
}