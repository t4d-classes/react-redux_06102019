import React from 'react';

import { CarForm } from './CarForm';

export const EditCarForm = ({ car, onSaveCar }) => {
  return <CarForm car={car} onSubmitCar={onSaveCar} buttonText="Save Car" />;
};
