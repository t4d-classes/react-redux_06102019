import React from 'react';
import PropTypes from 'prop-types';
import { pick } from 'lodash/fp';

import { carPropType as car } from '../propTypes/carPropType';
import { carTableConfigPropTypes } from '../propTypes/carTableConfigPropTypes';
import { useForm } from '../../Shared/hooks/useForm';

const carFormFields = pick(['make', 'model', 'year', 'color', 'price']);

export const CarEditRow = ({
  config,
  car,
  onSaveCar: saveCar,
  onCancelCar: cancelCar
}) => {

  const [ carForm, change ] = useForm({
    ...carFormFields(car),
  });

  return <tr>
    {config.map((c, i) => <td key={i}>
      <input type={c.type || 'text'} name={c.field} value={carForm[c.field]} onChange={change} />
    </td>)}
    <td>
      <button type="button" onClick={() => saveCar({ ...car, ...carForm })}>
        Save
      </button>
      <button type="button" onClick={cancelCar}>
        Cancel
      </button>
    </td>
  </tr>
};

CarEditRow.propTypes = {
  config: carTableConfigPropTypes,
  car,
  onSaveCar: PropTypes.func.isRequired,
  onCancelCar: PropTypes.func.isRequired,
};