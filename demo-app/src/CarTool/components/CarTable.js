import React from 'react';
import PropTypes from 'prop-types';
import { map } from 'lodash/fp';

import { carsPropType as cars } from '../propTypes/carsPropType';
import { carTableConfigPropTypes } from '../propTypes/carTableConfigPropTypes';
import { CarEditRow } from './CarEditRow';
import { CarViewRow } from './CarViewRow';

export const CarTable = ({
  config,
  cars,
  editCarId,
  onEditCar: editCar,
  onDeleteCar: deleteCar,
  onSaveCar: saveCar,
  onCancelCar:  cancelCar, 
  carEditRow: CarEditRow,
}) => {

  const carRowsMap = map(car => car.id === editCarId
    ? <CarEditRow key={car.id} car={car} onSaveCar={saveCar} onCancelCar={cancelCar} config={config} />
    : <CarViewRow key={car.id} car={car} onEditCar={editCar} onDeleteCar={deleteCar} config={config} />);

  return <table>
    <thead>
      <tr>
        {config.map((c, i) => <th key={i}>{c.caption}</th>)}
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {carRowsMap(cars)}
    </tbody>
  </table>;
};

CarTable.defaultProps = {
  carEditRow: CarEditRow,
};

CarTable.propTypes = {
  config: carTableConfigPropTypes,
  cars,
  editCarId: PropTypes.number,
  onEditCar: PropTypes.func.isRequired,
  onDeleteCar: PropTypes.func.isRequired,
  onSaveCar: PropTypes.func.isRequired,
  onCancelCar: PropTypes.func.isRequired,
  carEditRow: PropTypes.func,
};