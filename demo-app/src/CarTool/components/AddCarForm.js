import React from 'react';
import PropTypes from 'prop-types';

import { CarForm } from './CarForm';

export const AddCarForm = ({ onAddCar: addCar }) => {
  return <CarForm buttonText="Add Car" onSubmitCar={addCar} />;
};

AddCarForm.propTypes = {
  onAddCar: PropTypes.func.isRequired,
};