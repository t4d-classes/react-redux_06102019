import React, { useEffect } from 'react';

import { CarToolHeader } from './CarToolHeader';
import { CarTable } from './CarTable';
import { AddCarForm } from './AddCarForm';
import { EditCarFormRow } from './EditCarFormRow';

import { carsPropType as cars } from '../propTypes/carsPropType';

const carTableConfig = [
  { field: 'make', caption: 'Make' },
  { field: 'year', caption: 'Production Year', type:'number' },
  { field: 'color', caption: 'Body Color' },
  { field: 'price', caption: 'Cost', type:'number' },
  { field: 'model', caption: 'Model' },
];

export const CarTool = ({
  cars, editCarId,
  onRefreshCars: refreshCars,
  onAppendCar: addCar,
  onReplaceCar: saveCar,
  onDeleteCar: deleteCar,
  onEditCar: editCar,
  onCancelCar: cancelCar,
}) => {

  useEffect(() => {
    refreshCars();
  }, []);

  return <>
    <CarToolHeader version="1" />
    <CarTable cars={cars} editCarId={editCarId} config={carTableConfig}
      onEditCar={editCar} onDeleteCar={deleteCar}
      onSaveCar={saveCar} onCancelCar={cancelCar}
      carEditRow={EditCarFormRow} />
    <AddCarForm onAddCar={addCar} />
  </>;
};

CarTool.propTypes = {
  cars,
};