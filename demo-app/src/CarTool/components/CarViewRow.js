import React from 'react';
import PropTypes from 'prop-types';

import { carPropType as car } from '../propTypes/carPropType';
import { carTableConfigPropTypes } from '../propTypes/carTableConfigPropTypes';

export const CarViewRow = ({
  config,
  car,
  onDeleteCar: deleteCar,
  onEditCar: editCar
}) => {

  return <tr>
    {config.map((c, i) => <td key={i}>{car[c.field]}</td>)}
    <td>
      <button type="button" onClick={() => editCar(car.id)}>
        Edit
      </button>
      <button type="button" onClick={() => deleteCar(car.id)}>
        Delete
      </button>
    </td>
  </tr>
};

CarViewRow.propTypes = {
  config: carTableConfigPropTypes,
  car,
  onEditCar: PropTypes.func.isRequired,
  onDeleteCar: PropTypes.func.isRequired,
};