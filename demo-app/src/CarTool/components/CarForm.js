import React from 'react';
import PropTypes from 'prop-types';

import { useForm } from '../../Shared/hooks/useForm';
import { carPropType } from '../propTypes/carPropType';

const initCarForm = () => ({
  make: '',
  model: '',
  year: 1900,
  color: '',
  price: 0,
});

export const CarForm = ({
  buttonText,
  colorsLookUp,
  onSubmitCar,
  car,
}) => {

  const [ carForm, change, resetCarForm ] = useForm(car || initCarForm());

  const submitCar = () => {
    if (car) {
      onSubmitCar({ ...car, ...carForm });
    } else {
      onSubmitCar({ ...carForm });
    }
    resetCarForm();
  };

  return <form>
    <div>
      <label htmlFor="make-input">Make:</label>
      <input type="text" id="make-input" name="make"
        value={carForm.make} onChange={change} />
    </div>
    <div>
      <label htmlFor="model-input">Model:</label>
      <input type="text" id="model-input" name="model"
        value={carForm.model} onChange={change} />
    </div>
    <div>
      <label htmlFor="year-input">Year:</label>
      <input type="text" id="year-input" name="year"
        value={carForm.year} onChange={change} />
    </div>
    <div>
      <label htmlFor="color-input">Color:</label>
      <select id="color-input" name="color" value={carForm.color} onChange={change}>
        {colorsLookUp.map(color => <option key={color} value={color}>{color}</option>)}
      </select>
    </div>
    <div>
      <label htmlFor="price-input">Price:</label>
      <input type="text" id="price-input" name="price"
        value={carForm.price} onChange={change} />
    </div>
    <button type="button" onClick={submitCar}>{buttonText}</button>
  </form>;
};

CarForm.defaultProps = {
  buttonText: 'Submit Car',
  colorsLookUp: [ 'red', 'green', 'blue' ],
};

CarForm.propTypes = {
  buttonText: PropTypes.string,
  colorsLookUp: PropTypes.arrayOf(PropTypes.string),
  onSubmitCar: PropTypes.func.isRequired,
  car: carPropType,
};
