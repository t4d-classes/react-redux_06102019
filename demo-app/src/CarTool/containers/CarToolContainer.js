import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { refreshCars, appendCar, replaceCar, deleteCar, createEditCarAction, createCancelCarAction } from '../actions';
import { CarTool } from '../components/CarTool';

export const CarToolContainer = connect(
  ({ carTool: { cars, editCarId } }) => ({ cars, editCarId }),
  dispatch => bindActionCreators({
    onRefreshCars: refreshCars,
    onAppendCar: appendCar,
    onReplaceCar: replaceCar,
    onDeleteCar: deleteCar,
    onEditCar: createEditCarAction,
    onCancelCar: createCancelCarAction,
  }, dispatch),
)(CarTool);