const path = require('path');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const webpackConfig = require('./webpack.common');

module.exports = merge(webpackConfig, {
  mode: 'development',
  output: {
    filename: 'js/[name].bundle.js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './src/index.html',
    }),
  ],
  devServer: {
    contentBase: path.resolve(__dirname, '../src'),
    compress: true,
    port: 3000,
    watchContentBase: true,
    hot: true,
    publicPath: '/',
  }
});
