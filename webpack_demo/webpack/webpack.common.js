module.exports = {
  entry: {
    app: './src/js/index.js',
  },
  resolve: {
    extensions: ['.mjs', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-react'],
            // plugins: ['@babel/plugin-proposal-object-rest-spread'],
          }
        }
      },   
      {
        test: /\.scss$/,
        use: [
          // creates style nodes from JS strings
          { loader: 'style-loader' },
          // translates CSS into CommonJS
          { loader: 'css-loader', options: { sourceMap: true } },
          // compiles Sass to CSS
          { loader: 'sass-loader', options: { sourceMap: true } }, 
        ]
      }   
    ]
  },
  devtool: 'source-map',
};